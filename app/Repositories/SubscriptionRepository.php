<?php


namespace App\Repositories;


use App\Subscription;
use App\SubscriptionType;
use Carbon\Carbon;
use Transformer;

class SubscriptionRepository implements SubscriptionRepositoryInterface
{
    public function all($request){
        try {
            $query = self::filterSubscriptionIndexing($requests);

            $subscriptions = $query->get()->map->format();
            return response(['subscriptions' => $subscriptions]);
        } catch (\Exception $e) {
            return response(['message' => 'An Error has occured while fetching subscriptions'], 500);
        }
    }

    public function create($userId, $subscriptionTypeName){
        try {
            $user = self::locateUser($userId);
            if ($user) {
                $subscriptions = self::getUserActivesubscriptions($userId);
                if (!self::checkIfActiveSubscriptionOfTypeExists($subscriptions, $subscriptionTypeName)) {
                    $data = [
                        'user_id' => $userId,
                        'price' => self::determinePrice($subscriptions, $subscriptionTypeName),
                        'from' => Carbon::now()->toDateTimeString(),
                        'to' => Carbon::now()->addYear()->toDateTimeString(),
                        'type' => $subscriptionTypeName,
                    ];
                    $this->persistSubscription($data);
                    return response(['message' => 'Subscription of type \'' . $subscriptionTypeName . '\' for user: ' . $userId . ' has been created successfully!']);
                } else {
                    return response(['message' => 'Subscription of type \'' . $subscriptionTypeName . '\' already exists for user: ' . $userId]);
                }
            }
            return response(['message' => 'Subscription could not be completed for user: ' . $userId]);
        } catch (\Exception $e) {
            return response(['message' => 'An Error has occured while creating subscription'], 500);
        }
    }

    private static function getUsersFromExternalAPI(){
        $client = new \GuzzleHttp\Client();
        $response = $client->get('http://ilias.users.challenge.dev.monospacelabs.com/users');

        return json_decode($response->getBody()->getContents());
    }

    private static function locateUser($userId){
        $users = self::getUsersFromExternalAPI();
        if ($users){
            $users = Transformer::stdToArray($users);
            foreach ($users as $user){
                if ($user['id'] == trim($userId)){
                    if ($user['active']){
                        return $user;
                    }
                }
            }
        }
        return null;
    }

    private static function getUserActivesubscriptions($userId){
        $subscriptions = Subscription::with('subscriptionType')
            ->where('user_id', $userId)
            ->where('from', '<', Carbon::now()->toDateTimeString())
            ->where('to', '>', Carbon::now()->toDateTimeString())
            ->get();

        return $subscriptions;
    }

    private static function checkIfActiveSubscriptionOfTypeExists($subscriptions, $typeName){
        foreach ($subscriptions as $subscription){
            if ($subscription->subscriptionType->name == $typeName){
                return true;
            }
        }
        return false;
    }

    private static function determinePrice($subscriptions, $subscriptionTypeName){
        $subscriptionType = SubscriptionType::where('name', $subscriptionTypeName)->first();
        $price = $subscriptionType->price;
        foreach ($subscriptions as $subscription){
            $price *= 0.7;
        }

        return $price;
    }

    private function persistSubscription($data){
        $subscription = new Subscription();
        $subscription->subscription_type_id = SubscriptionType::where('name', $data['type'])->first()->id;
        $subscription->fill($data);
        $subscription->save();
    }

    private static function filterSubscriptionIndexing($request){
        $query = Subscription::with('subscriptionType');
        if ($request->has('active')){
            $query ->where('from', '<', Carbon::now()->toDateTimeString())
                ->where('to', '>', Carbon::now()->toDateTimeString());
        }
        if ($request->has('past')){
            $query ->where('from', '>=', Carbon::now()->toDateTimeString())
                ->where('to', '<=', Carbon::now()->toDateTimeString());
        }
        if ($request->has('from')) {
            $query ->where('from', '<', Carbon::parse($request->from)->toDateTimeString());
        }
        if ($request->has('to')) {
            $query ->where('to', '>', Carbon::parse($request->to)->toDateTimeString());
        }

        return $query;
    }
}
