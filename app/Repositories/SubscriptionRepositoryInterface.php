<?php

namespace App\Repositories;

interface SubscriptionRepositoryInterface
{
    public function all($filters);

    public function create($userId, $subscriptionType);
}
