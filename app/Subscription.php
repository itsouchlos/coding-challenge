<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $fillable = ['user_id', 'price', 'from', 'to'];

    public function subscriptionType(){
        return $this->belongsTo('App\SubscriptionType', 'subscription_type_id');
    }

    public function format(){
        return [
            'type' => $this->subscriptionType->name,
            'price' => $this->price,
            'from' => date('d-m-Y', strtotime($this->from)),
            'to' => date('d-m-Y', strtotime($this->to)),
            'user_id' => $this->user_id,
        ];
    }
}
