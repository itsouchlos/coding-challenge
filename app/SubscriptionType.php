<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionType extends Model
{
    protected $table = 'subscriptions_types';

    protected $fillable = ['name', 'price'];

    public function subscriptions(){
        return $this->hasMany('App\Subscription', 'subscription_type_id');
    }
}
