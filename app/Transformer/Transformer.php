<?php


namespace App\Transformer;


class Transformer
{
    public function stdToArray($stdObject){
        return json_decode(json_encode($stdObject), true);
    }
}
