<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\SubscriptionType;
use Faker\Generator as Faker;

$factory->define(SubscriptionType::class, function (Faker $faker) {
    return [
        'name' => $faker->word(),
        'price' => mt_rand(500, 1500),
    ];
});
