<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('subscription_type_id');
            $table->string('user_id');
            $table->unsignedFloat('price')->default(0.0);
            $table->dateTime('from')->nullable();
            $table->dateTime('to')->nullable();
            $table->timestamps();

            $table->foreign('subscription_type_id')->references('id')->on('subscriptions_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptions');
    }
}
