<?php

use Illuminate\Database\Seeder;

class SubscriptionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = $this->getAllTypes();
        foreach ($types as $type){
            DB::table('subscriptions_types')->insert([
                'name' => $type,
            ]);
        }
    }
}
