<?php

namespace Tests\Feature;

use App\Subscription;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\SubscriptionType;

class SubscriptionTest extends TestCase
{
    use RefreshDatabase;

    public function testCreationOfInactiveUser()
    {
        $userId = '5f0ed06d7e97a72e03dd9941';

        factory(SubscriptionType::class, 5)->create();

        $response = $this->postJson('/api/subscription', [
            'user_id' => $userId,
            'subscription_type' => SubscriptionType::first()->name
        ]);

        $response->assertJson(['message' => 'Subscription could not be completed for user: '.$userId]);
        $response->assertStatus(200);
    }

    public function testCreationOfActiveUserWithNoSubscriptions()
    {
        $userId = '5f0ed06d7e97a72e03dd9942';

        factory(SubscriptionType::class, 5)->create();

        $subscriptionTypeName = SubscriptionType::first()->name;

        $response = $this->postJson('/api/subscription', [
            'user_id' => $userId,
            'subscription_type' => $subscriptionTypeName
        ]);

        $response->assertJson(['message' => 'Subscription of type \''.$subscriptionTypeName.'\' for user: '.$userId.' has been created successfully!']);
        $response->assertStatus(200);
        $this->assertDatabaseCount('subscriptions', 1);
    }

    public function testCreationOfActiveUserWithOneMoreSubscription()
    {
        $userId = '5f0ed06d7e97a72e03dd9942';

        factory(SubscriptionType::class, 5)->create();

        $subscriptionType1 = SubscriptionType::find(1);
        $subscriptionType2 = SubscriptionType::find(2);
        $subscriptionTypeName1 = $subscriptionType1->name;
        $subscriptionTypeName2 = $subscriptionType2->name;

        $response = $this->postJson('/api/subscription', [
            'user_id' => $userId,
            'subscription_type' => $subscriptionTypeName1
        ]);

        $response->assertJson(['message' => 'Subscription of type \''.$subscriptionTypeName1.'\' for user: '.$userId.' has been created successfully!']);
        $response->assertStatus(200);
        $this->assertDatabaseCount('subscriptions', 1);

        $response = $this->postJson('/api/subscription', [
            'user_id' => $userId,
            'subscription_type' => $subscriptionTypeName2
        ]);

        $response->assertJson(['message' => 'Subscription of type \''.$subscriptionTypeName2.'\' for user: '.$userId.' has been created successfully!']);
        $response->assertStatus(200);
        $this->assertDatabaseCount('subscriptions', 2);

        $subscription1 = Subscription::find(1);
        $subscription2 = Subscription::find(2);
        $this->assertEquals($subscriptionType1->price, $subscription1->price);
        $this->assertEquals($subscriptionType2->price * 0.7, $subscription2->price);
    }
}
